/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.HashAttributeSet;

/**
 *
 * @author luis
 */
public class Cargar {
   public static Path path = Paths.get("src/recursos/jugadores.csv");
    
    public static  ArrayList<Usuario> leerInformacionUsuarios() throws FileNotFoundException, IOException{
        ArrayList<Usuario> usuarios = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(path.toFile()))){
            String linea = null;
            int contador = 0;
            while((linea = br.readLine())!=null){ 
                if (contador>0){ // para evitar el salto de linea
                    String [] campo = linea.split(",");        
                    Usuario user = new Usuario(campo[0],campo[1],
                            Integer.valueOf(campo[2]),Integer.valueOf(campo[3]));
                    user.setPalabramax(campo[4]);
                    user.setPuntajepalabra(Integer.valueOf(campo[5]));
                    usuarios.add(user);                    
                }
                contador++;   
            }
           return usuarios;
        }
    }
    public static  HashMap<Integer,Nivel> leerInformacionNiveles() throws FileNotFoundException, IOException{
        HashMap<Integer,Nivel> dic = new  HashMap<>();
        Path path = Paths.get("src/recursos/niveles.csv");
        try(BufferedReader br = new BufferedReader(new FileReader(path.toFile()))){
            String linea = null;
            int contador = 0;
            while((linea = br.readLine())!=null){ 
                if (contador>0){ // para evitar el salto de linea
                    String [] campo = linea.split(",");        
                    dic.put(Integer.valueOf(campo[0]), new Nivel(campo[1],campo[2]));
                }
                contador++;   
            }
           return dic;
        }
    }
    
    public static  ArrayList<String> leerInformacioPalabras(String name) throws FileNotFoundException, IOException{
        ArrayList<String> palabras = new ArrayList<>();
        Path path = Paths.get("src/recursos/"+name+".csv");
        try(BufferedReader br = new BufferedReader(new FileReader(path.toFile()))){
            String linea = null;

            while((linea = br.readLine())!=null){ 
                
                    String [] campo = linea.split(",");                        
                    palabras.add(campo[0].replace("\"", "").toUpperCase());
               
            }
           return palabras;
        }
    }
    
    public static void agregarusuario(ArrayList<Usuario> usuarios ){
        String csvfile = "src/recursos/jugadores.csv";
        
        try (BufferedWriter wr = new BufferedWriter(new FileWriter(csvfile))){
            wr.write("jugador,idioma,nivel,puntaje,palabra,puntajepalabra");
            wr.newLine();
            for (Usuario u: usuarios){
                wr.write(u.escribir());
                wr.newLine();
            }
            
            } 
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
    }
    
}
