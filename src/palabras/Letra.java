/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 *
 * @author luis
 */
public class Letra {

    private StackPane stack;
    private String letra;
    private Label word;
    private Button bt;
    private Circle circulo;
    private boolean peligrosa,premiada,seleccionada;

    public Letra(String letra) {
        this.letra = letra;
        stack = new StackPane();

        bt = new Button();
        Color color = Color.BLUE;
        Font fuente = Font.font("Comic Sans", FontWeight.BOLD,FontPosture.REGULAR, 20);
        
        word = new Label(letra);
        word.setFont(fuente);
        word.setTextFill(Color.DARKBLUE);
        stack.getChildren().add(bt);
        stack.getChildren().add(word);
        estadonormal();

    }

    public void estadonormal() {
        seleccionada = false;
        bt.setStyle(
                "-fx-background-color: linear-gradient(#5BD8F2,#7153DD);\n"
                + "    -fx-background-radius: 12;\n"
                + "    -fx-background-insets: 0;\n"
                + "    -fx-font-size: 12px;\n"
                + "    -fx-padding: 52 52 20 20;\n"
                + "    -fx-text-fill: red;\n"
                + "}");
        stack.getChildren().remove(circulo);
    }

    public void estadoRojo() {
        bt.setStyle(
                "-fx-background-color: linear-gradient(#E9326F,#ED7A4E);\n"
                + "    -fx-background-radius: 12;\n"
                + "    -fx-background-insets: 0;\n"
                + "    -fx-font-size: 12px;\n"
                + "    -fx-padding: 52 52 20 20;\n"
                + "    -fx-text-fill: red;\n"
                + "}");

    }

    public void agregarCirculo() {
        circulo = new Circle(30);
        Color color = Color.AQUA;
        circulo.setFill(color);
        stack.getChildren().remove(word);
        stack.getChildren().addAll(circulo, word);
        seleccionada = true;
    }
    
    public void agregardorada(){
        bt.setStyle(
                "-fx-background-color: linear-gradient(#e9d217,#e9d217);\n"
                + "    -fx-background-radius: 12;\n"
                + "    -fx-background-insets: 0;\n"
                + "    -fx-font-size: 12px;\n"
                + "    -fx-padding: 52 52 20 20;\n"
                + "    -fx-text-fill: red;\n"
                + "}");
        premiada = true;
    }
    
    public void changeL(String g){
        word.setText(g);
        setLetra(g);    
    }

    public StackPane getStack() {
        return stack;
    }

    public void setStack(StackPane stack) {
        this.stack = stack;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public Label getWord() {
        return word;
    }

    public void setWord(Label word) {
        this.word = word;
    }

    public Button getBt() {
        return bt;
    }

    public void setBt(Button bt) {
        this.bt = bt;
    }

    public Circle getCirculo() {
        return circulo;
    }

    public void setCirculo(Circle circulo) {
        this.circulo = circulo;
    }

    public boolean isPeligrosa() {
        return peligrosa;
    }

    public void setPeligrosa(boolean peligrosa) {
        this.peligrosa = peligrosa;
    }

    public boolean isPremiada() {
        return premiada;
    }
    public boolean isCOPADA(){
        if (peligrosa && seleccionada){
            return true;
        }
        else if (peligrosa){
            return true;
        }
        else if(seleccionada){
            return true;
        }
        else{
            return false;
        }
    }

    public void setPremiada(boolean premiada) {
        this.premiada = premiada;
    }

    public boolean isSeleccionada() {
        return seleccionada;
    }

    public void setSeleccionada(boolean seleccionada) {
        this.seleccionada = seleccionada;
    }
    
}
