/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import static palabras.Palabras.scene;
import static palabras.PanelControl.niveles;

/**
 *
 * @author luis
 */
public class Sistema {
    private Button enviar,salir;
    private BorderPane root; 
    private GridPane malla;
    private VBox info;
    private Label nivel,puntaje,palabra,puntajepalabra;
    private String idioma;
    private ArrayList<String> palabras,lyric; 
    private String palabraconstruida;
    private ArrayList<String> palabrascreadas;
    private ArrayList<Integer> puntajesporpalabra;
    private ArrayList<Letra> letras;
    private int cont_puntajes,nivelavanza,contador,puntaje_palabra,velocidad,puntajenivel;
    private Random rnd = new Random();
    private Usuario usuario;
    private boolean PREMIADA,activada;
    private Letra anterior= null;
    public static boolean dead; // cuando se haga true se acaba el hilo 
    
    public Sistema(Usuario usuario){
        this.usuario = usuario;
        this.idioma= usuario.getIdioma();
        this.nivelavanza = usuario.getNivel();
        setearConfiNivel(niveles.get(nivelavanza));
        this.cont_puntajes = usuario.getPuntaje();
        try {
            palabras = Cargar.leerInformacioPalabras(idioma);
        } catch (IOException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        levantar();
        CrearPalabras();
        Thread hilo_peligrosas = new Thread(new Peligrosa());
        hilo_peligrosas.start();
    }
    public void levantar(){
        contador = 0;
        activada = true;
        root = new BorderPane();
        letras = new ArrayList<>();
        puntajesporpalabra = new ArrayList<>();
        palabrascreadas = new ArrayList<>();
        palabraconstruida ="";
        salir = new Button("Salir");
        salir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                usuario.setPalabramax(palabramayorpuntaje());
                usuario.setPuntajepalabra(mayorpuntaje());
                usuario.setNivel(nivelavanza);
                usuario.setPuntaje(cont_puntajes);
                
                dead = true;
                
                scene.setRoot(Palabras.panel.getRoot());
            }
        });
        enviar = new Button("enviar");
        
        enviar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                contador = 0;
                if(BuscarPalabras(palabraconstruida)){
                    System.out.println("Gano");    
                    palabrascreadas.add(palabraconstruida);
                    darlepuntaje(palabraconstruida);
                    puntajesporpalabra.add(puntaje_palabra);
                    actualizarALenviar();  
                    CrearPalabras();
                    //moverrows();
                }
                else{
                    System.out.println("Perdio");
                    System.out.println(palabraconstruida);
                }
                regresarestadoincial();
            }
        });

        malla = new GridPane();
        
        for(int i =0 ; i<5 ; i ++){
            for (int j = 0; j<5; j++){
                Letra l = new Letra(" ");
                letras.add(l);
                
                l.getStack().setOnMouseClicked(new EventHandler<MouseEvent>() {
                    
                    public void handle(MouseEvent event) {
                        if (!l.isSeleccionada()){
                        
                        if(contador >0){
                            
                            if(validarPOSICION(l, anterior)){  // VALIDACION DE LETRAS VECINAS ???????? CONFIRMAR 
                                
                                palabraconstruida+= l.getLetra();
                                l.agregarCirculo();
                                if(l.isPremiada()){
                                    PREMIADA = true;
                                }
                                anterior = l;
                                //contador_indices+=1;
                            }
                            
                        }
                        else
                        {
                                palabraconstruida+= l.getLetra();
                                l.agregarCirculo();
                                if(l.isPremiada()){
                                    PREMIADA = true;
                                }   
                            anterior = l;
                            }
                        contador++;
                            
                            
                    }  
                    }
                });
                malla.add(l.getStack(),j, i);
                
            }
        }
        
        
                    
        
        info = new VBox();

        info.
        setStyle("-fx-background-image: url('"+Constantes.ruta+"/fondo.jpg');"
               + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+(Constantes.ancho)+" "+(Constantes.alto)+"; "
                + "-fx-background-position: center center;");
        Font fuente = Font.font("veranda", FontWeight.BOLD,FontPosture.REGULAR, 22);
        
        nivel = new Label("Nivel "+nivelavanza);
        puntaje = new Label(""+cont_puntajes);
        palabra = new Label();
        puntajepalabra = new  Label();
        nivel.setFont(fuente);puntaje.setFont(fuente);palabra.setFont(fuente);
        puntajepalabra.setFont(fuente);
        nivel.setTextFill(Color.DARKBLUE);puntaje.setTextFill(Color.DARKBLUE);
        palabra.setTextFill(Color.BLACK);puntajepalabra.setTextFill(Color.BLACK);
        Insets in = new Insets(80,100, 80, 100);
        VBox vacio = new VBox();
        vacio.setPadding(in);
        info.setSpacing(18);
        info.setAlignment(Pos.CENTER);
        info.getChildren().addAll(salir,nivel,puntaje,vacio,palabra,puntajepalabra,enviar);
        root.setLeft(malla);
        root.setRight(info);
    }

   

    public BorderPane getRoot() {
        return root;
    }

    
    public void setMalla(GridPane malla) {
        this.malla = malla;
    }

    public VBox getInfo() {
        return info;
    }

    public void setInfo(VBox info) {
        this.info = info;
    }
    
    public void darlepuntaje(String palabra){
        int sizee = palabra.length();
        if (sizee<4 ){
            puntaje_palabra = 50; 
        }
        else if(sizee>=4 && sizee<6){
            puntaje_palabra = 75;
        }
        else if(sizee>=6 && sizee<8){
            puntaje_palabra = 100;
        }
        else{
            puntaje_palabra= 150;
        }
    }
    
    public void actualizarALenviar(){
        
        if(PREMIADA){
           puntajepalabra.setText(puntaje_palabra+"**"); 
        }else
        {
        puntajepalabra.setText(puntaje_palabra+""); 
       
        }
        cont_puntajes+=puntaje_palabra;
        puntaje.setText(""+cont_puntajes);
    }
    public boolean validarPOSICION(Letra letra,Letra anterior){
        
        if(anterior!=null){
            
                int index = letras.indexOf(letra); 
                int indexanterior = letras.indexOf(anterior);
                 int [][] matriz = {{0,5,10,15,20},
                                     {1,6,11,16,21},
                                     {2,7,12,17,22},
                                     {3,8,13,18,23},
                                     {4,9,14,19,24}};
                 int i =0 ; int j = 0; // posiciones actuales
                 int i1 =0 ; int j1 = 0; // posiciones viejas
                 for (int fila = 0 ; fila < 5 ; fila ++){
                     for (int col = 0 ; col < 5 ; col ++){
                     if (matriz[fila][col] == index){
                         j = fila;
                         i = col;
                     }
                     if (matriz[fila][col] == indexanterior){
                         j1 = fila;
                         i1 = col;
                     }
                 }
                 }

                 //VALIDAR PARA FILAS Y COLUMNAS
                 System.out.println(i+","+j+" "+i1+","+j1);

                 if(i-i1 == 0 && j-j1==0){
                     
                     return true;
                 }
                 else if(Math.abs(i-i1) == 1 && j-j1==0){
                      return true;
                 }
                 else if(Math.abs(j-j1) == 1 && i-i1==0){
                      return true;
                 }else if(Math.abs(i-i1) == 1 && Math.abs(j-j1) ==1){
                      return true;
                 }
            }
        
        return false;
    }
    public void moverrows(){
        int [] col1 = new int[] {0,5,10,15,20};
        int [] col2 = new int[] {1,6,11,16,21};
        int [] col3 = new int[] {2,7,12,17,22};
        int [] col4 = new int[] {3,8,13,18,23};
        int [] col5 = new int[] {4,9,14,19,24};
        moverUnaColumn(col1);
        moverUnaColumn(col2);
        moverUnaColumn(col3);
        moverUnaColumn(col4);
        moverUnaColumn(col5);
    }
    
    public void moverUnaColumn(int [] row ){
       int contador = 0;
       ArrayList<Letra> nomod = new ArrayList<>();
       ArrayList<Letra> nuevo = new ArrayList<>();
       for (int i=0; i < 5; i++){
           if(letras.get(row[i]).isSeleccionada()){
               contador+=1;
           }
           else{
               nomod.add(letras.get(row[i]));
           }
       }
       int sinmodificar = 5 -contador; 
       for(int i= 0; i<contador;i++){ // cantidad de letras nuevas q se deben crear
           nuevo.add(new Letra(" "));
       }
       for(Letra l : nomod){
           System.out.println(l.getLetra());
           nuevo.add(l);
       }
       for(int i=0; i < 5; i++) {
           letras.add(row[i], nuevo.get(i));
       }
    }
  
            
    public boolean BuscarPalabras(String palabrita){
        if(palabras.contains(palabrita)){
            return true;
        }
        return false;
    }
    
    public void CrearPalabras(){
        /*int pos = rnd.nextInt(palabras.size()); // primero a escoger la palabra aleatoria de todas las palabras
        
        palabraelegida = palabras.get(pos);
        
        palabra.setText(palabraelegida);
        
        */
        
        // IMPORTANTE PALABRAS ES LA LISTA CARGADA DEL ARCHIVO del ideoma 
        lyric = new ArrayList<>(); 
        for(int i=0; i < letras.size() ;i++){                   //este codigo es para llenar la matriz de palabras
            String l = palabras.get(rnd.nextInt(palabras.size()));
            for (int s = 0; s< l.length();s ++){
                lyric.add(Character.toString(l.charAt(s)));
            }
        }
        for(Letra l :letras){
            l.changeL(lyric.get(rnd.nextInt(lyric.size())));  // hasta aqui se llena 
        }
        
        // se agrega la palabra escogida en la sopa de letras  
        /*ArrayList<Integer> numeros = new ArrayList<>();
        for (int s = 0; s< palabraelegida.length();s ++){ // recorriendo cada indice
            int num = rnd.nextInt(letras.size());
            while(numeros.contains(num)){ // codigo que me asegura que se escriba toda la palabra
                num = rnd.nextInt(letras.size());       
            }
            numeros.add(num);
            letras.get(num).changeL(Character.toString(palabraelegida.charAt(s)));
        }*/
        //darlepuntaje(palabraelegida);
        //puntajepalabra.setText(puntaje_palabra+ " pts");
        
    }
    
    public void regresarestadoincial(){
        for(Letra l : letras){
            l.estadonormal();
        }
        palabraconstruida = "";
    }
    
    
    /// ESTAS TRES FUNCIONES ES CODIGO PARA CREAR BLOQUES ROJOS Y DORADOS 
    public int generarDisponible(){
        ArrayList <Integer> letritas = new ArrayList<>();
        int contador = 0;
        for (Letra l: letras){
            if(!l.isCOPADA()){
                letritas.add(contador);
            }
            contador++;
            
        }
        int numrnd = rnd.nextInt(letritas.size());
        return letritas.get(numrnd);    
    }
    
    public void generarPremiada(){
        int indicernd = generarDisponible();
        Letra letra = letras.get(indicernd);
        letra.setPremiada(true);
        letra.agregardorada();
    }
    
    public void generarpeligrosa(){
        int indicernd = generarDisponible();
        Letra letra = letras.get(indicernd);
        letra = letras.get(indicernd);
        letra.setPeligrosa(true);
        letra.estadoRojo();
    }
    
   public class Peligrosa implements Runnable{
       int contador;
  
       public void run(){
           
            
            //debemos usar una variable de control para cerrar los hilos al finalizar todo osea dead
         
            while(!dead){  
                contador+=1; 
                Platform.runLater(()->{
                    actualizarnivel();
                   if (contador % velocidad == 0){ // significa que cada 10 seg se hara una letra roja
                       generarpeligrosa();
                       
                   }
                   if (contador % 50 == 0){ // significa que cada 50 seg se hara una letra DORADA
                       generarPremiada();
                   }
                });

                try{
                    Thread.sleep(1000);
                }catch (InterruptedException ex) {
                    System.out.println("Algo inesperado ocurrió");
                }
               
            }
        }
   }
 
   
   public void actualizarnivel(){
       if(cont_puntajes>puntajenivel){
           System.out.println("actualizado");
           nivelavanza++;
           setearConfiNivel(niveles.get(nivelavanza));
           nivel.setText("Nivel "+nivelavanza);
       }
   }
   
   public void setearConfiNivel(Nivel nivel){
       this.velocidad = nivel.getVelocidad_peligrosa();
       this.puntajenivel = nivel.getPuntos();
   }
   
   public int mayorpuntaje(){
       if(puntajesporpalabra.size()>0){
       return  Collections.max(puntajesporpalabra); // should return 7
        }
       return 0;
   }
   public String palabramayorpuntaje(){ 
       if (palabrascreadas.size()>0){
       Integer maxIdx = puntajesporpalabra.indexOf(mayorpuntaje()); 
       return palabrascreadas.get(maxIdx);
        
       }
       return "";
   }
}
