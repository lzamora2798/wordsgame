/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.layout.GridPane;
import static palabras.Palabras.scene;
import palabras.PanelControl;
import static palabras.PanelControl.usuarios;
/**
 *
 * @author luis
 */
public class PanelUsuarios {
    private GridPane root;
    
    
    public PanelUsuarios(){
        levantar();
    }
    public void levantar(){
        
      
        root = new GridPane();
        root.setStyle( "-fx-background-color: white;");
        root.setHgap(16); //espacio horizontal de gridPane
        root.setVgap(26);
        root.setPadding(new Insets(10));
        
        int size = (usuarios.size()/5)+1;
        int u = 0;
        for(int i = 0;i<size ; i++){
            for(int j = 0; j<4; j++){  
                if(u<usuarios.size()){
                    Usuario user = usuarios.get(u);
                    
                    Button entrar = new Button(user.toString());
                    entrar.setStyle(
                        "    -fx-background-color: black;\n" +
                        "    -fx-background-radius: 20;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 14px;\n"+
                        "    -fx-text-fill: white;\n" +
                        "}");
                    entrar.setOnAction(new EventHandler<ActionEvent>() {
                       
                        public void handle(ActionEvent event) {
                            user.setSistema(new Sistema(user));
                            scene.setRoot(user.getSistema().getRoot());
                        }
                    });
                    root.add(entrar,j,i);
                }
                u++;
            }
        }
        
        
        
    }

    public GridPane getRoot() {
        return root;
    }

    public void setRoot(GridPane root) {
        this.root = root;
    }





}
