/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

/**
 *
 * @author Usuario
 */
public class Nivel {
    private int puntos;
    private int velocidad_peligrosa;
    public Nivel(String puntos, String velocidad){
        this.puntos = Integer.valueOf(puntos);
        this.velocidad_peligrosa = Integer.valueOf(velocidad);
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getVelocidad_peligrosa() {
        return velocidad_peligrosa;
    }

    public void setVelocidad_peligrosa(int velocidad_peligrosa) {
        this.velocidad_peligrosa = velocidad_peligrosa;
    }

    @Override
    public String toString() {
        return "Nivel{" + "puntos=" + puntos + ", velocidad_peligrosa=" + velocidad_peligrosa + '}';
    }
    
    
}
