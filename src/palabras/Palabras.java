/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import static palabras.PanelControl.usuarios;

/**
 *
 * @author luis
 */
public class Palabras extends Application {
    public static Scene scene;
    public static PanelControl panel;
    @Override
    public void start(Stage primaryStage) {
        panel = new PanelControl();
        
        scene = new Scene(panel.getRoot(),Constantes.ancho,Constantes.alto    );
        
        primaryStage.setTitle("Juego de Palabras");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        super.stop(); //To change body of generated methods, choose Tools | Templates.
        Cargar.agregarusuario(usuarios);
        Sistema.dead = true;
    }
    
    
    
    
}
