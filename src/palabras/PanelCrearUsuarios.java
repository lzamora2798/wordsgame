/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import static palabras.Palabras.scene;
import static palabras.PanelControl.usuarios;
/**
 *
 * @author luis
 */
public class PanelCrearUsuarios {
    private VBox root;
    private ComboBox ideomas; 
    private TextField boxnombre;
    private  ArrayList<String> catg;
    private Button crear,limpiar; 
    private Usuario user;
    public PanelCrearUsuarios(){
        crearArray();
        levantarpanel();
    
    }
    public void levantarpanel(){
        root = new VBox();
        HBox title = new HBox();
        title.getChildren().add(new Label("Crear un nuevo usuario"));
        title.setAlignment(Pos.CENTER);
        
        HBox nombres = new HBox();
        boxnombre = new TextField();
        nombres.getChildren().addAll(new Label("  Ingrese nombre"),boxnombre);
        nombres.setSpacing(40);
        nombres.setAlignment(Pos.TOP_LEFT);
        
        HBox catt = new HBox ();
        catt.setSpacing(40);
        ObservableList<String> options = FXCollections.observableArrayList(catg);
        ideomas = new ComboBox(options);
        catt.getChildren().addAll(new Label("  Escoja un ideoma"),ideomas);
        
        HBox botons = new HBox();
        crear = new Button("Crear");
        crear.setOnAction(new EventHandler<ActionEvent>() {
          
            public void handle(ActionEvent event) {
              String opidioma = (String)ideomas.getValue();
              String name = boxnombre.getText();
              if (!name.equals("") && !opidioma.equals("")){
                  user = new Usuario(name, opidioma);
                  usuarios.add(user);
                  user.setSistema(new Sistema(user));
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setContentText("Creado con exito! de click en OK\n");
                    alert.showAndWait();
                    scene.setRoot(user.getSistema().getRoot());
              }
              else{
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setContentText("No ingreso datos validos");
                    alert.showAndWait();
              }
            }
        });
        limpiar = new Button("Limpiar");
        limpiar.setOnAction(new EventHandler<ActionEvent>() {
          
            public void handle(ActionEvent event) {
               boxnombre.setText("");
               ideomas.setValue(catg.get(0));
            }
        });
        
        
        botons.getChildren().addAll(crear,limpiar);
        botons.setSpacing(40);
        botons.setPadding(new Insets(50));
        botons.setAlignment(Pos.CENTER);
        root.getChildren().addAll(title,nombres,catt,botons);
        root.setSpacing(30);
        
        
    }
    public void crearArray(){
        catg = new ArrayList<>();
        catg.add("espanol");
        catg.add("ingles");
        catg.add("italiano");
    }

    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }

    public ComboBox getIdeomas() {
        return ideomas;
    }

    public void setIdeomas(ComboBox ideomas) {
        this.ideomas = ideomas;
    }

    public TextField getBoxnombre() {
        return boxnombre;
    }

    public void setBoxnombre(TextField boxnombre) {
        this.boxnombre = boxnombre;
    }

    public ArrayList<String> getCatg() {
        return catg;
    }

    public void setCatg(ArrayList<String> catg) {
        this.catg = catg;
    }
    
    
}
