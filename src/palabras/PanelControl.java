/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import static palabras.Palabras.scene;



/**
 *
 * @author CltControl
 */
public class PanelControl {
   private VBox root;
    private HBox raizPrimeraP;
    private Button iniciar, ingresarP, salir;
    public static Thread hiloTiempo ;
    private PanelUsuarios panelusuario;
    private PanelCrearUsuarios panelcrearusuario;
    public static ArrayList<Usuario> usuarios = new ArrayList<>();
    public static HashMap<Integer,Nivel> niveles;
    public PanelControl(){
       
           root = new VBox();
           root.setStyle("-fx-background-image: url"
                   + "('"+Constantes.ruta+"/mainfondo.jpg');"
                   + "-fx-background-repeat: stretch;"
                   + "-fx-background-size: "+Constantes.ancho+" "+Constantes.alto+"; "
                           + "-fx-background-position: center center;");
           try {
            usuarios = Cargar.leerInformacionUsuarios();
            niveles = Cargar.leerInformacionNiveles();
        } catch (IOException ex) {
            Logger.getLogger(PanelUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
           creacionBotones();
           clickEnBotones();
           
    }
    
    
    //metodo para crear los botones de la pantalla principal
    public void creacionBotones(){
        
        //inicializamos los botones
        iniciar = new Button("Iniciar");
        iniciar.setStyle(
                        "    -fx-background-color: black;\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 24px;\n" +
                        "    -fx-text-fill:  white;\n" +
                        "}");
        ingresarP = new Button("Puntajes");
        ingresarP.setStyle(
                        "    -fx-background-color: black;\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 24px;\n" +
                        "    -fx-text-fill:  white;\n" +
                        "}");
        salir = new Button("Salir");
        salir.setStyle(
                        "    -fx-background-color: black;\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 24px;\n"+
                        "    -fx-text-fill: white;\n" +
                        "}");
        //aplicando estilos (cambiar luego por hojas de estilo)
        //agregar espacio entre el contenedor y los elementos internos
        root.setPadding(new Insets(20));
        
        //agregar espacio horizontal entre los elementos
        root.setSpacing(15);
        
        //alineacion de los botones al centro
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(iniciar, ingresarP, salir);
        
    }
    //método para manejar las acciones de los botones
    public void clickEnBotones(){
        iniciar.setOnAction(new EventHandler<ActionEvent>(){
            public void handle (ActionEvent event){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText(null);
                alert.setTitle("Partidas");
                alert.setContentText("Seleccione una opcion");
               
                ButtonType nueva = new ButtonType("Crear partida");
                ButtonType vieja = new ButtonType("Cargar partida");
                alert.getButtonTypes().clear(); 
                alert.getButtonTypes().addAll(nueva,vieja);
 
                Optional<ButtonType> option= alert.showAndWait();
            
                if (option.get() == nueva) {
                   panelcrearusuario= new PanelCrearUsuarios();
                   scene.setRoot(panelcrearusuario.getRoot());
                } 
                if(option.get() == vieja){
                   panelusuario = new PanelUsuarios();
                   scene.setRoot(panelusuario.getRoot());
                }
            }
        });
        
        ingresarP.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
                Table tabla = new Table();
                scene.setRoot(tabla.getRoot());
            }
        });
        salir.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
               Platform.exit();
            }
        });
    }

    public VBox getRoot() {
        return root;
    } 
}
