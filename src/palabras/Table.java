/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import static palabras.Palabras.scene;
import static palabras.PanelControl.usuarios;


/**
 *
 * @author luis
 */
public class Table {
    private VBox root;
    private TableView tabla;
    private Button salir;
    
    public Table(){
        crearperfil();
    }
    public void crearperfil(){
        root = new VBox();
        salir = new Button("Regresar");
        root.setStyle("-fx-background-image: url('"+Constantes.ruta+"/fondo_table.jpg');"
               + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+550+" "+455+";"
                + "-fx-background-position: center center;");
        HBox titulo = new HBox();
        titulo.setAlignment(Pos.CENTER);
        titulo.setStyle("-fx-background-color: grey;\n -fx-pading: 20; ");
        Font fuente = Font.font("veranda", FontWeight.BOLD,FontPosture.REGULAR, 18);
        Label labe = new Label("Historial de Jugadores");
        labe.setFont(fuente);
        labe.setTextFill(Color.WHITE);
        titulo.getChildren().add(labe);
        
        tabla = new TableView();
        TableColumn nombre = new TableColumn("Nombre");
        nombre.setMinWidth(80);
        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        TableColumn fecha = new TableColumn("Idioma");
        fecha.setMinWidth(80);
        fecha.setCellValueFactory(new PropertyValueFactory<>("idioma"));
        
        TableColumn points = new TableColumn("Nivel");
        points.setMinWidth(60);
        points.setCellValueFactory(new PropertyValueFactory("nivel"));
        
        TableColumn pala = new TableColumn("PalabraMax");
        pala.setMinWidth(100);
        pala.setCellValueFactory(new PropertyValueFactory("palabramax"));
        TableColumn pptpa = new TableColumn("Puntaje Palabra");
        pptpa.setMinWidth(80);
        pptpa.setCellValueFactory(new PropertyValueFactory("puntajepalabra"));
        
        TableColumn level= new TableColumn("Puntos");
        level.setMinWidth(100);
        level.setCellValueFactory(new PropertyValueFactory<>("puntaje"));
        tabla.getColumns().addAll(nombre,fecha,points,level,pala,pptpa);
        salir.setStyle("#{\n" +
                        "    -fx-background-color: linear-gradient(#ff5400, #be1d00);\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 18px;\n"+
                        "    -fx-text-fill:  black;\n" +
                        "}");
        salir.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
               scene.setRoot(Palabras.panel.getRoot());
            }
        });
        root.setAlignment(Pos.CENTER);
        tabla.getItems().addAll(usuarios);
        root.getChildren().addAll(titulo,tabla,salir);
        
    }
    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }

    public TableView getTabla() {
        return tabla;
    }

    public void setTabla(TableView tabla) {
        this.tabla = tabla;
    }
    
}
