/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palabras;

/**
 *
 * @author luis
 */
public class Usuario {
    private String nombre;
    private String idioma;
    private Sistema sistema;
    private String palabramax;
    private int nivel=1;
    private int puntaje;
    private int puntajepalabra;
    public Usuario(String nombre, String idioma) {
        this.nombre = nombre;
        this.idioma = idioma;
        
    }
    public Usuario(String nombre, String idioma,int nivel, int puntaje) {
        this.nombre = nombre;
        this.idioma = idioma;
        this.nivel = nivel;
        this.puntaje = puntaje;
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    @Override
    public String toString() {
        return "nombre: " + nombre + "\nidioma: " + idioma + "\nnivel: " + nivel + 
                "\npuntaje: " + puntaje;
    }

    public String getPalabramax() {
        return palabramax;
    }

    public void setPalabramax(String palabramax) {
        this.palabramax = palabramax;
    }

    public int getPuntajepalabra() {
        return puntajepalabra;
    }

    public void setPuntajepalabra(int puntajepalabra) {
        this.puntajepalabra = puntajepalabra;
    }
    
    public String escribir(){
        return nombre+","+idioma+","+nivel+","+puntaje+","+palabramax+","+puntajepalabra;
    }
    
}
